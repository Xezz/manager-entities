package de.xezzco.manager.entity;

import javax.persistence.*;
import java.util.List;

/**
 * User: Xezz
 * Date: 26.10.2014
 * Time: 00:49
 */
@Entity(name = "Position")
public class Position {

    /**
     * The database id for this Position
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The String identifying this Position
     */
    private String name;

    // One Position is used by many Players
    /**
     * The List of Players that occupy this Position
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "position")
    private List<Player> players;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
}

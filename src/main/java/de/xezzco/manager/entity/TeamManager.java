package de.xezzco.manager.entity;

import javax.persistence.*;
import java.util.List;

/**
 * User: Xezz
 * Date: 26.10.2014
 * Time: 00:53
 */
@Entity(name = "Manager")
public class TeamManager {

    /**
     * The database id for this TeamManager
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Name of the TeamManager
     */
    private String managerName;

    /**
     * The name the TeamManger gave his Team
     */
    private String teamName;

    /**
     * List of all Players this TeamManager has in his team
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "manager")
    private List<Player> players;

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
}

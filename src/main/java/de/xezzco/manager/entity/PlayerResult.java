package de.xezzco.manager.entity;

import javax.persistence.*;

/**
 * User: Xezz
 * Date: 26.10.2014
 * Time: 01:01
 */
@Entity(name = "PlayerResult")
public class PlayerResult {

    /**
     * The database id for this PlayerResult
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The match day in the season
     */
    private Integer matchDay;

    /**
     * The grade by kicker the player received
     */
    private Double grade;

    /**
     * The minute when the player entered the game range 0-90
     */
    private Integer minuteOfEntry;

    /**
     * The minute when the player left the game range 0-90
     */
    private Integer minuteOfLeave;

    /**
     * The amount of goals the player scored (including penalties)
     */
    private Integer amountOfGoals;

    /**
     * The amount of goals the player scored via penalties
     */
    private Integer penaltyGoals;

    /**
     * The amount of goals the player assisted
     */
    private Integer assists;

    /**
     * Not sure yet TODO
     */
    private Integer scorerPoints;

    /**
     * Amount of red cards the player received. Range 0-1
     */
    private Integer redCards;

    /**
     * Amount of double yellow -> red cards the player received. Range 0-1
     */
    private Integer redYellowCards;

    /**
     * Amount of yellow cards the player received. Range 0-1 TODO check this
     */
    private Integer yellowCards;

    /**
     * The amount of goals the opponent scored for this game. TODO could move this to its own table
     */
    private Integer amountOfOpponentGoals;

    /**
     * The actual points a manager receives for nominating this player for this matchday
     */
    private Integer gamePoints;

    /**
     * The Player this PlayerResult is associated with
     */
    @ManyToOne
    @JoinColumn(name = "spieler_id")
    private Player thePlayer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMatchDay() {
        return matchDay;
    }

    public void setMatchDay(Integer matchDay) {
        this.matchDay = matchDay;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }

    public Integer getMinuteOfEntry() {
        return minuteOfEntry;
    }

    public void setMinuteOfEntry(Integer minuteOfEntry) {
        this.minuteOfEntry = minuteOfEntry;
    }

    public Integer getMinuteOfLeave() {
        return minuteOfLeave;
    }

    public void setMinuteOfLeave(Integer minuteOfLeave) {
        this.minuteOfLeave = minuteOfLeave;
    }

    public Integer getAmountOfGoals() {
        return amountOfGoals;
    }

    public void setAmountOfGoals(Integer amountOfGoals) {
        this.amountOfGoals = amountOfGoals;
    }

    public Integer getPenaltyGoals() {
        return penaltyGoals;
    }

    public void setPenaltyGoals(Integer penaltyGoals) {
        this.penaltyGoals = penaltyGoals;
    }

    public Integer getAssists() {
        return assists;
    }

    public void setAssists(Integer assists) {
        this.assists = assists;
    }

    public Integer getScorerPoints() {
        return scorerPoints;
    }

    public void setScorerPoints(Integer scorerPoints) {
        this.scorerPoints = scorerPoints;
    }

    public Integer getRedCards() {
        return redCards;
    }

    public void setRedCards(Integer redCards) {
        this.redCards = redCards;
    }

    public Integer getRedYellowCards() {
        return redYellowCards;
    }

    public void setRedYellowCards(Integer redYellowCards) {
        this.redYellowCards = redYellowCards;
    }

    public Integer getYellowCards() {
        return yellowCards;
    }

    public void setYellowCards(Integer yellowCards) {
        this.yellowCards = yellowCards;
    }

    public Player getThePlayer() {
        return thePlayer;
    }

    public void setThePlayer(Player thePlayer) {
        this.thePlayer = thePlayer;
    }

    public Integer getAmountOfOpponentGoals() {
        return amountOfOpponentGoals;
    }

    public void setAmountOfOpponentGoals(Integer amountOfOpponentGoals) {
        this.amountOfOpponentGoals = amountOfOpponentGoals;
    }

    public Integer getGamePoints() {
        return gamePoints;
    }

    public void setGamePoints(Integer gamePoints) {
        this.gamePoints = gamePoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlayerResult that = (PlayerResult) o;

        if (amountOfGoals != null ? !amountOfGoals.equals(that.amountOfGoals) : that.amountOfGoals != null)
            return false;
        if (amountOfOpponentGoals != null ? !amountOfOpponentGoals.equals(that.amountOfOpponentGoals) : that.amountOfOpponentGoals != null)
            return false;
        if (assists != null ? !assists.equals(that.assists) : that.assists != null) return false;
        if (gamePoints != null ? !gamePoints.equals(that.gamePoints) : that.gamePoints != null) return false;
        if (grade != null ? !grade.equals(that.grade) : that.grade != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!matchDay.equals(that.matchDay)) return false;
        if (minuteOfEntry != null ? !minuteOfEntry.equals(that.minuteOfEntry) : that.minuteOfEntry != null)
            return false;
        if (minuteOfLeave != null ? !minuteOfLeave.equals(that.minuteOfLeave) : that.minuteOfLeave != null)
            return false;
        if (penaltyGoals != null ? !penaltyGoals.equals(that.penaltyGoals) : that.penaltyGoals != null) return false;
        if (redCards != null ? !redCards.equals(that.redCards) : that.redCards != null) return false;
        if (redYellowCards != null ? !redYellowCards.equals(that.redYellowCards) : that.redYellowCards != null)
            return false;
        if (scorerPoints != null ? !scorerPoints.equals(that.scorerPoints) : that.scorerPoints != null) return false;
        if (!thePlayer.equals(that.thePlayer)) return false;
        if (yellowCards != null ? !yellowCards.equals(that.yellowCards) : that.yellowCards != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + matchDay.hashCode();
        result = 31 * result + (grade != null ? grade.hashCode() : 0);
        result = 31 * result + (minuteOfEntry != null ? minuteOfEntry.hashCode() : 0);
        result = 31 * result + (minuteOfLeave != null ? minuteOfLeave.hashCode() : 0);
        result = 31 * result + (amountOfGoals != null ? amountOfGoals.hashCode() : 0);
        result = 31 * result + (penaltyGoals != null ? penaltyGoals.hashCode() : 0);
        result = 31 * result + (assists != null ? assists.hashCode() : 0);
        result = 31 * result + (scorerPoints != null ? scorerPoints.hashCode() : 0);
        result = 31 * result + (redCards != null ? redCards.hashCode() : 0);
        result = 31 * result + (redYellowCards != null ? redYellowCards.hashCode() : 0);
        result = 31 * result + (yellowCards != null ? yellowCards.hashCode() : 0);
        result = 31 * result + (amountOfOpponentGoals != null ? amountOfOpponentGoals.hashCode() : 0);
        result = 31 * result + (gamePoints != null ? gamePoints.hashCode() : 0);
        result = 31 * result + thePlayer.hashCode();
        return result;
    }

    public boolean isSame(PlayerResult o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (amountOfGoals != null ? !amountOfGoals.equals(o.amountOfGoals) : o.amountOfGoals != null)
            return false;
        if (amountOfOpponentGoals != null ? !amountOfOpponentGoals.equals(o.amountOfOpponentGoals) : o.amountOfOpponentGoals != null)
            return false;
        if (assists != null ? !assists.equals(o.assists) : o.assists != null) return false;
        if (gamePoints != null ? !gamePoints.equals(o.gamePoints) : o.gamePoints != null) return false;
        if (grade != null ? !grade.equals(o.grade) : o.grade != null) return false;
        if (!matchDay.equals(o.matchDay)) return false;
        if (minuteOfEntry != null ? !minuteOfEntry.equals(o.minuteOfEntry) : o.minuteOfEntry != null)
            return false;
        if (minuteOfLeave != null ? !minuteOfLeave.equals(o.minuteOfLeave) : o.minuteOfLeave != null)
            return false;
        if (penaltyGoals != null ? !penaltyGoals.equals(o.penaltyGoals) : o.penaltyGoals != null) return false;
        if (redCards != null ? !redCards.equals(o.redCards) : o.redCards != null) return false;
        if (redYellowCards != null ? !redYellowCards.equals(o.redYellowCards) : o.redYellowCards != null)
            return false;
        if (scorerPoints != null ? !scorerPoints.equals(o.scorerPoints) : o.scorerPoints != null) return false;
        if (!thePlayer.equals(o.thePlayer)) return false;
        if (yellowCards != null ? !yellowCards.equals(o.yellowCards) : o.yellowCards != null) return false;

        return true;
    }
}

package de.xezzco.manager.entity;


import javax.persistence.*;
import java.util.List;

/**
 * User: Xezz
 * Date: 26.10.2014
 * Time: 00:29
 */
@Entity
@Table(name = "Verein")
public class Club {

    /**
     * The database id for this Club
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The Club's name
     */
    private String name;

    /**
     * The URL to this Club's player detail page
     */
    private String url;

    /**
     * The List of Player's that this Club has under contract
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "club")
    private List<Player> players;

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

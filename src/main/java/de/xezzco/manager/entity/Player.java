package de.xezzco.manager.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * User: Xezz
 * Date: 26.10.2014
 * Time: 00:19
 * <p/>
 * This entity represents a player of a club
 * He has a name and a surname and a URL associated to himself
 * He is also a member of a club (VEREIN), has a position (POSITION) and
 * MIGHT be part of the team of a MANAGER
 */
@Entity(name = "Spieler")
public class Player implements Serializable {

    /**
     * The database id for this Player
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The first name of the Player, can span several first names separated by whitespaces or connected by dashes
     */
    private String firstName;

    /**
     * the surname of the Player, can span several surnames separated by whitespaces or connected by dashes
     */
    private String surName;

    /**
     * The URL to the detail page of this Player as a String
     */
    private String url;

    /**
     * The club this player plays for
     */
    @ManyToOne
    @JoinColumn(name = "verein_id")
    private Club club;

    /**
     * The position this Player employs
     */
    @ManyToOne
    @JoinColumn(name = "position_id")
    private Position position;

    /**
     * The TeamManager that has this Player under "contract"
     */
    @ManyToOne
    @JoinColumn(name = "manager_id")
    private TeamManager manager;

    /**
     * All the PlayerResult's this Player is associated with
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "thePlayer")
    private List<PlayerResult> results;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public TeamManager getManager() {
        return manager;
    }

    public void setManager(TeamManager manager) {
        this.manager = manager;
    }

    public List<PlayerResult> getResults() {
        return results;
    }

    public void setResults(List<PlayerResult> results) {
        this.results = results;
    }
}

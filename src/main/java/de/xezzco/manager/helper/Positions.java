package de.xezzco.manager.helper;

/**
 * User: Xezz
 * Date: 29.10.2014
 * Time: 20:00
 * Mapping of Position description Strings to an Enum
 */
public enum Positions {
    TORWART("Torwart"), ABWEHR("Abwehr"), MITTELFELD("Mittelfeld"), STURM("Sturm");
    private final String position;

    private Positions(String position) {
        this.position = position;
    }

    public static Positions getPositionByDescription(final String description) {
        if (null == description) {
            throw new IllegalArgumentException("Null as description not allowed");
        }
        for (Positions p : values()) {
            if (p.position.toLowerCase().equals(description.toLowerCase())) {
                return p;
            }
        }
        throw new IllegalArgumentException(String.format("No Position with the description: %s found", description));
    }

    public String getDescriptiveName() {
        return this.position;
    }

}
